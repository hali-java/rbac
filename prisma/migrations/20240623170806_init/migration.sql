-- AddForeignKey
ALTER TABLE `RoleAuthority` ADD CONSTRAINT `RoleAuthority_authorityId_fkey` FOREIGN KEY (`authorityId`) REFERENCES `Authority`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
