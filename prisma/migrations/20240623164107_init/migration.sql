-- AddForeignKey
ALTER TABLE `RoleAuthority` ADD CONSTRAINT `RoleAuthority_roleId_fkey` FOREIGN KEY (`roleId`) REFERENCES `Role`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
