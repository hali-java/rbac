-- AlterTable
ALTER TABLE `authority` ADD COLUMN `roleId` VARCHAR(191) NULL;

-- AddForeignKey
ALTER TABLE `Authority` ADD CONSTRAINT `Authority_roleId_fkey` FOREIGN KEY (`roleId`) REFERENCES `Role`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
