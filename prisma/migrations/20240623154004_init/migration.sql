/*
  Warnings:

  - You are about to drop the column `component` on the `authority` table. All the data in the column will be lost.
  - You are about to drop the column `icon` on the `authority` table. All the data in the column will be lost.
  - You are about to drop the column `key` on the `authority` table. All the data in the column will be lost.
  - You are about to drop the column `parentId` on the `authority` table. All the data in the column will be lost.
  - You are about to drop the column `path` on the `authority` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `authority` DROP COLUMN `component`,
    DROP COLUMN `icon`,
    DROP COLUMN `key`,
    DROP COLUMN `parentId`,
    DROP COLUMN `path`;
