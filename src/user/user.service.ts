import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateUserDto, UserLoginDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {

  @Inject()
  private prisma: PrismaService;



  async login(loginUserDto: UserLoginDto) {
    const user = await this.prisma.user.findFirst({
      where: {
        username: loginUserDto.username
      },
      include: {
        UserRole: {
          select: {
            role: true
          }
        }
      }
    })

    if (!user) {
      throw new HttpException('用户不存在', HttpStatus.ACCEPTED);
    }

    if (user.password !== loginUserDto.password) {
      throw new HttpException('密码错误', HttpStatus.ACCEPTED);
    }

    return user;
  }

  async findRolesByIds(roleIds: string[]) {
    return this.prisma.role.findMany({
      where: {
        id: {
          in: roleIds
        }
      },
      include: {
        RoleAuthority: {
          include: {
            Authority: true
          }
        }
      }
    })
  }

}
